# Exemplo docker

###### Este tutorial tem como objetivo, relembrar a parte prática passada em aula, se restringindo somente a isso.

## Preparação
Toda imagem docker é criada com base em outra imagem, ou seja, antes de começar a criar algo é necessário que você escolha uma imagem para se basear, caso você não possua imagens locais você pode usar algumas das públicas do [docker hub](https://hub.docker.com/).

No nosso caso vamos usar a imagem `openjdk:8-jdk-alpine` que já vem com o java instalado e tem como base o `alpine` que é um sistema leve e **recomendado** para imagens docker.

O arquivo `Dockerfile` é o **responsável por guardar todas as intruções que serão usadas para criar a sua imagem**, para o nosso caso, somente vamos precisar de 3 instruções, mas você pode consultar a documentação de todas as instruções possiveis [aqui](https://docs.docker.com/engine/reference/builder/).

Veja como ficou o nosso docker file no final:


```docker
FROM openjdk:8-jdk-alpine

COPY . .

CMD ["java", "-jar", "estacionamento-0.0.1-SNAPSHOT.jar"]
```

Para criar nossa imagem útilizamos o comando:
- docker build -t \<nome final da imagem\> .

Vale a pena lembrar que esse comando foi **executado na mesma pasta que o Dockerfile**.

## Gerenciamento das instancias

O gerenciamento das imagens é algo mais simples e direto, por isso, essa parte do tutorial será mais pragmática.

### Iniciar uma instancia
Para iniciar uma instancia em background use:
`docker run -d <nome da imagem>`

Lembrando que você pode passar o argumento `-p <porta local>:<porta do docker>` Para que a porta interna do seu container, seja 'redirecionada' para a porta local do seu computador.

### Listar instancias
Para listar as instancias sendo executadas use:
`docker ps`

Para acessar os logs de uma instancia use:
`docker logs <nome ou id do container>`

Lembrando que você pode usar o argumento `-f` para entrar no modo `follow` dos logs que permite que você veja os logs em tempo real.

### Desligar instancias
Para desligar uma instancia use:
`docker kill <nome ou id do container>`

